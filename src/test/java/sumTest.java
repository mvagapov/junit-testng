import Calc.Calc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class sumTest extends BaseTest {
    private static Stream<Arguments> sumDataProviderInt() {
        return Stream.of(
                Arguments.of(15, 10, 25),
                Arguments.of(-15, -10, -25),
                Arguments.of(0, 10, 10)
        );
    }

    private static Stream<Arguments> sumDataProviderDouble() {
        return Stream.of(
                Arguments.of(30.55, 25.25, 55.75),
                Arguments.of(-30.55, -25.25, 55.75),
                Arguments.of(0, 25.25, 25.25)
        );
    }

    @ParameterizedTest
    @MethodSource("sumDataProviderInt")
    public void sumCalcTest(int var1, int var2, int result) {
        Assertions.assertEquals(result, calc.sum(var1, var2), "Incorrect epta");
    }

    @ParameterizedTest
    @MethodSource("sumDataProviderDouble")
    public void sumCalcTest(double var1, double var2, double result) {
        Assertions.assertEquals(result, calc.sum(var1, var2), "Incorrect epta");
    }

}
