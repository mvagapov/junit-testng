import Calc.Calc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class multiplyTest extends BaseTest {
    private static Stream<Arguments> multiplyDataProviderInt() {
        return Stream.of(
                Arguments.of(5, 10, 50),
                Arguments.of(-5, -10, 50),
                Arguments.of(0, 10, 0)
        );
    }

    private static Stream<Arguments> multiplyDataProviderDouble() {
        return Stream.of(
                Arguments.of(2, 0.5, 1),
                Arguments.of(-2, -0.5, 1),
                Arguments.of(0, 25.25, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("multiplyDataProviderInt")
    public void multiplyCalcTest(int var1, int var2, int result) {
        Assertions.assertEquals(result, calc.multiply(var1, var2), "Incorrect epta");
    }

    @ParameterizedTest
    @MethodSource("multiplyDataProviderDouble")
    public void multiplyCalcTest(double var1, double var2, double result) {
        Assertions.assertEquals(result, calc.multiply(var1, var2), "Incorrect epta");
    }

}
