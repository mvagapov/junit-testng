import Calc.Calc;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

public class BaseTest {
    protected Calc calc = null;
    @BeforeEach
    public void startUp(){
        System.out.println("Test is started");
        calc = new Calc();
        //System.out.println("Calc is successfully created");

    }

    @AfterEach
    public void shutDown(){
        System.out.println("Test is finished");
        calc = null;
        //System.out.println("Calc is successfully destroyed");
    }
}
