import Calc.Calc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class divideTest extends BaseTest {
    private static Stream<Arguments> divideDataProviderInt() {
        return Stream.of(
                Arguments.of(50, 10, 5),
                Arguments.of(-50, -10, 5),
                Arguments.of(5, 0, 0)
        );
    }

    private static Stream<Arguments> divideDataProviderDouble() {
        return Stream.of(
                Arguments.of(2, 0.5, 4),
                Arguments.of(-2, -0.5, 4),
                Arguments.of(25.25, 0, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("divideDataProviderInt")
    public void divideCalcTest(int var1, int var2, int result) {
        if (var2 == 0){
            throw new ArithmeticException("division by zero");
        }
        Assertions.assertEquals(result, calc.divide(var1, var2), "Incorrect epta");
    }

    @ParameterizedTest
    @MethodSource("divideDataProviderDouble")
    public void divideCalcTest(double var1, double var2, double result) {
        if (var2 == 0){
            throw new ArithmeticException("division by zero");
        }
        Assertions.assertEquals(result, calc.divide(var1, var2), "Incorrect epta");
    }

}