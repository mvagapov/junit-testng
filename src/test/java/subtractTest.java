import Calc.Calc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class subtractTest extends BaseTest {

    // sum int, double (+)
    // subtract int, double (-)
    // multiply int, double (*)
    // divide int, double (/)

    private static Stream<Arguments> subtractDataProviderInt() {
        return Stream.of(
                Arguments.of(15, 10, 5),
                Arguments.of(-15, -10, -5),
                Arguments.of(0, 10, -10)
        );
    }

    private static Stream<Arguments> subtractDataProviderDouble() {
        return Stream.of(
                Arguments.of(30.55, 25.25, 5.300000000000001),
                Arguments.of(-30.55, -25.25, -5.300000000000001),
                Arguments.of(0, 25.25, -25.25)
        );
    }

    @ParameterizedTest
    @MethodSource("subtractDataProviderInt")
    public void subtractCalcTest(int var1, int var2, int result) {
        Assertions.assertEquals(result, calc.subtract(var1, var2), "Incorrect");

    }

    @ParameterizedTest
    @MethodSource("subtractDataProviderDouble")
    public void subtractCalcTest(double var1, double var2, double result) {
        Assertions.assertEquals(result, calc.subtract(var1, var2), "Incorrect");

    }


}
